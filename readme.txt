This will place a link above your node edit body 'add lorem text'
If you click it it will add lorem text to the node body.
But if you are using FCKeditor you would need to turn that off first
by clicking 'Switch to plain text editor'
You can turn it on again after that.

You can see a video of it working here
http://blip.tv/file/4715778

